#include "stdafx.h"
#include "DrawWindow.h"
ATOM RegisterDrawWindow(HINSTANCE hInst);
LRESULT CALLBACK DrawWindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
HWND CreateChildWindow(HWND hMDIClient,int numOfWindow);
ATOM RegisterDrawWindow(HINSTANCE hInst)
{
	WNDCLASS wndClass;
	ZeroMemory(&wndClass, sizeof(wndClass));
	wndClass.style = CS_HREDRAW | CS_VREDRAW;
	wndClass.hInstance = hInst;
	wndClass.hCursor = LoadCursor(hInst, IDC_ARROW);
	wndClass.hIcon = LoadIcon(NULL, MAKEINTRESOURCE(IDI_MYAPPS));
	wndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = L"TEXT_WINDOW";
	wndClass.cbWndExtra = 0;
	wndClass.lpfnWndProc = (WNDPROC)DrawWindowProc;
	return RegisterClass(&wndClass);
}

LRESULT CALLBACK DrawWindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_CREATE:
		return 0;
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		case IDM_FILE_NEW:
		case IDM_FILE_OPEN:
		case IDM_FILE_SAVE:
			SendMessage(GetParent(GetParent(hWnd)), WM_COMMAND, wParam, lParam);
			break;
		default:
			break;
		}
	}
	default:
		break;
	}
	return DefMDIChildProc(hWnd, msg, wParam, lParam);
}

HWND CreateChildWindow(HWND hMDIClient,int numOfWindows)
{
	WCHAR szName[100];
	wsprintf(szName, L"Noname-%d.drw",numOfWindows);
	MDICREATESTRUCT mdiCrStr;
	mdiCrStr.hOwner = GetModuleHandle(NULL);
	mdiCrStr.szClass = L"TEXT_WINDOW";
	mdiCrStr.cx = CW_USEDEFAULT;
	mdiCrStr.cy = CW_USEDEFAULT;
	mdiCrStr.x = CW_USEDEFAULT;
	mdiCrStr.y = CW_USEDEFAULT;
	mdiCrStr.szTitle = szName;
	mdiCrStr.style = NULL;
	mdiCrStr.lParam = NULL;
	return (HWND)SendMessage(hMDIClient, WM_MDICREATE, 0, (LPARAM)(LPMDICREATESTRUCT)&mdiCrStr);
}
