#pragma once
#include "resource.h"
ATOM RegisterDrawWindow(HINSTANCE hInst);
LRESULT CALLBACK DrawWindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
HWND CreateChildWindow(HWND hMDIClient, int numOfWindow);